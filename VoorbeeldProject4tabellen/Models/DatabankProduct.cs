﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Web;

namespace VoorbeeldProject4tabellen.Models
{
    public partial class DatabankProduct : DbContext
    {
        public DatabankProduct() : base("name=DatabankEntities")
        {
        }

        public virtual DbSet<Product> Products { get; set; }
    }
}