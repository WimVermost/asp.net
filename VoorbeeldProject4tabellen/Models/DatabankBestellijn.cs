﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Web;

namespace VoorbeeldProject4tabellen.Models
{
    public partial class DatabankBestellijn : DbContext
    {
        public DatabankBestellijn() : base("name=DatabankEntities")
        {
        }

        public virtual DbSet<Bestellijn> Bestellijnen { get; set; }

        public DbSet<Bestelling> Bestellings { get; set; }
        public DbSet<Product> Products { get; set; }
    }
   
}
