﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace VoorbeeldProject4tabellen.Models
{
    public class Product
    {
        public int ID { get; set; }
        public string beschrijving { get; set; }
        public decimal prijs { get; set; }
    }
}