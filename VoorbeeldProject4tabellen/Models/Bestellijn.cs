﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace VoorbeeldProject4tabellen.Models
{
    public class Bestellijn
    {
        public int ID { get; set; }
        public int aantal { get; set; }
        public int bestellingID { get; set; }
        public virtual Bestelling bestelling { get; set; }
        public int productId { get; set; }
        public virtual Product product { get; set; }
    }
}