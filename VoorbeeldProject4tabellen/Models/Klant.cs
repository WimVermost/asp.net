﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace VoorbeeldProject4tabellen.Models
{
    public class Klant
    {
        public int ID { get; set; }
        public string naam { get; set; }
        public string email { get; set; }
        public virtual List<Bestelling> Bestellingen{ get; set; }
    }
}