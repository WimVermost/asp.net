﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace VoorbeeldProject4tabellen.Models
{
    public class Bestelling
    {
        public int ID { get; set; }
        public int KlantId { get; set; }
        public virtual Klant klant { get; set; }
    }
}