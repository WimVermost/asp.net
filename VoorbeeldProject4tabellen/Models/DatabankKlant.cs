﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Web;

namespace VoorbeeldProject4tabellen.Models
{
    public partial class DatabankKlant : DbContext
    {
        public DatabankKlant() : base("name=DatabankEntities")
        {
        }

        public virtual DbSet<Klant> Klants { get; set; }
    }
}