﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Web;

namespace VoorbeeldProject4tabellen.Models
{
    public partial class DatabankBestelling : DbContext
    {
        public DatabankBestelling() : base("name=DatabankEntities")
        {
        }

        public virtual DbSet<Bestelling> Bestellingen { get; set; }

        public DbSet<Klant> Klants { get; set; }
    }
}
