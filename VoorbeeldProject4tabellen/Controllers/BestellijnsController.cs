﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using VoorbeeldProject4tabellen.Models;

namespace VoorbeeldProject4tabellen.Controllers
{
    public class BestellijnsController : Controller
    {
        private DatabankBestellijn db = new DatabankBestellijn();

        // GET: Bestellijns
        public ActionResult Index()
        {
            var bestellijnen = db.Bestellijnen.Include(b => b.bestelling).Include(b => b.product);
            return View(bestellijnen.ToList());
        }

        // GET: Bestellijns/Details/5
        public ActionResult Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Bestellijn bestellijn = db.Bestellijnen.Find(id);
            if (bestellijn == null)
            {
                return HttpNotFound();
            }
            return View(bestellijn);
        }

        // GET: Bestellijns/Create
        public ActionResult Create()
        {
            ViewBag.bestellingID = new SelectList(db.Bestellings, "ID", "ID");
            ViewBag.productId = new SelectList(db.Products, "ID", "beschrijving");
            return View();
        }

        // POST: Bestellijns/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create([Bind(Include = "ID,aantal,bestellingID,productId")] Bestellijn bestellijn)
        {
            if (ModelState.IsValid)
            {
                db.Bestellijnen.Add(bestellijn);
                db.SaveChanges();
                return RedirectToAction("Index");
            }

            ViewBag.bestellingID = new SelectList(db.Bestellings, "ID", "ID", bestellijn.bestellingID);
            ViewBag.productId = new SelectList(db.Products, "ID", "beschrijving", bestellijn.productId);
            return View(bestellijn);
        }

        // GET: Bestellijns/Edit/5
        public ActionResult Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Bestellijn bestellijn = db.Bestellijnen.Find(id);
            if (bestellijn == null)
            {
                return HttpNotFound();
            }
            ViewBag.bestellingID = new SelectList(db.Bestellings, "ID", "ID", bestellijn.bestellingID);
            ViewBag.productId = new SelectList(db.Products, "ID", "beschrijving", bestellijn.productId);
            return View(bestellijn);
        }

        // POST: Bestellijns/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit([Bind(Include = "ID,aantal,bestellingID,productId")] Bestellijn bestellijn)
        {
            if (ModelState.IsValid)
            {
                db.Entry(bestellijn).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            ViewBag.bestellingID = new SelectList(db.Bestellings, "ID", "ID", bestellijn.bestellingID);
            ViewBag.productId = new SelectList(db.Products, "ID", "beschrijving", bestellijn.productId);
            return View(bestellijn);
        }

        // GET: Bestellijns/Delete/5
        public ActionResult Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Bestellijn bestellijn = db.Bestellijnen.Find(id);
            if (bestellijn == null)
            {
                return HttpNotFound();
            }
            return View(bestellijn);
        }

        // POST: Bestellijns/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            Bestellijn bestellijn = db.Bestellijnen.Find(id);
            db.Bestellijnen.Remove(bestellijn);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
