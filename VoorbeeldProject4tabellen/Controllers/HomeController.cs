﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace VoorbeeldProject4tabellen.Controllers
{
    public class HomeController : Controller
    {
        public ActionResult Index()
        {
            return View();
        }

        public ActionResult About()
        {
            ViewBag.Message = "Wat kan mijn applicatie allemaal?";

            return View();
        }

        public ActionResult Contact()
        {
            ViewBag.Message = "Hier kan je ons contacteren";

            return View();
        }
    }
}