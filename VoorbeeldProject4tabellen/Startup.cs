﻿using Microsoft.Owin;
using Owin;

[assembly: OwinStartupAttribute(typeof(VoorbeeldProject4tabellen.Startup))]
namespace VoorbeeldProject4tabellen
{
    public partial class Startup
    {
        public void Configuration(IAppBuilder app)
        {
            ConfigureAuth(app);
        }
    }
}
