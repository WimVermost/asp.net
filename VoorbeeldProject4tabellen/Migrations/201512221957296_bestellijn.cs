namespace VoorbeeldProject4tabellen.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class bestellijn : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.Bestellijns",
                c => new
                    {
                        ID = c.Int(nullable: false, identity: true),
                        aantal = c.Int(nullable: false),
                        bestellingID = c.Int(nullable: false),
                        productId = c.Int(nullable: false),
                    })
                .PrimaryKey(t => t.ID)
                .ForeignKey("dbo.Bestellings", t => t.bestellingID, cascadeDelete: true)
                .ForeignKey("dbo.Products", t => t.productId, cascadeDelete: true)
                .Index(t => t.bestellingID)
                .Index(t => t.productId);
            
            CreateTable(
                "dbo.Products",
                c => new
                    {
                        ID = c.Int(nullable: false, identity: true),
                        beschrijving = c.String(),
                        prijs = c.Decimal(nullable: false, precision: 18, scale: 2),
                    })
                .PrimaryKey(t => t.ID);
            
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.Bestellijns", "productId", "dbo.Products");
            DropForeignKey("dbo.Bestellijns", "bestellingID", "dbo.Bestellings");
            DropIndex("dbo.Bestellijns", new[] { "productId" });
            DropIndex("dbo.Bestellijns", new[] { "bestellingID" });
            DropTable("dbo.Products");
            DropTable("dbo.Bestellijns");
        }
    }
}
