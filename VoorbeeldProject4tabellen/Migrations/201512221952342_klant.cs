namespace VoorbeeldProject4tabellen.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class klant : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.Klants",
                c => new
                    {
                        ID = c.Int(nullable: false, identity: true),
                        naam = c.String(),
                        email = c.String(),
                    })
                .PrimaryKey(t => t.ID);
            
            CreateTable(
                "dbo.Bestellings",
                c => new
                    {
                        ID = c.Int(nullable: false, identity: true),
                        KlantId = c.Int(nullable: false),
                    })
                .PrimaryKey(t => t.ID)
                .ForeignKey("dbo.Klants", t => t.KlantId, cascadeDelete: true)
                .Index(t => t.KlantId);
            
            DropTable("dbo.Products");
        }
        
        public override void Down()
        {
            CreateTable(
                "dbo.Products",
                c => new
                    {
                        ID = c.Int(nullable: false, identity: true),
                        beschrijving = c.String(),
                        prijs = c.Decimal(nullable: false, precision: 18, scale: 2),
                    })
                .PrimaryKey(t => t.ID);
            
            DropForeignKey("dbo.Bestellings", "KlantId", "dbo.Klants");
            DropIndex("dbo.Bestellings", new[] { "KlantId" });
            DropTable("dbo.Bestellings");
            DropTable("dbo.Klants");
        }
    }
}
